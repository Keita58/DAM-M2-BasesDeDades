= Programació en bases de dades
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

== Activitat funcions PL/pgSQL sobre sakila

Utilitzarem la base de dades link:db/postgresql/sakila.sql[sakila].

=== Exercicis

[NOTE]
====
Per a cada exercici es vol:

- el codi que crea la funció.
- un exemple de com s'utilitza.
- el resultat obtingut.
====

1. Fes una funció que rebi com a paràmetres dues durades en minuts i compti la
quantitat de pel·lícules que tenen una durada que està entre aquestes dues. La
funció retornarà aquesta quantitat. Si no n'hi ha cap, a més, mostrarà un missatge.

2. Crea una funció que rebi el nom i cognom d'un actor o actriu i que retorni
totes les dades de les pel·lícules on ha participat. Ha de mostrar errors
tant si no existeix l'actor com si no ha fet cap pel·lícula.

3. Fes una funció que pugui afegir un idioma a una pel·lícula en concret. Es passarà
per paràmetres el títol de la pel·lícula i l'idioma. La funció informarà dels diferents
errors que poden sortir.
