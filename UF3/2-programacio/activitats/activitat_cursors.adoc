= Programació en bases de dades
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

== Activitat cursors

Utilitzarem la base de dades link:db/postgresql/worldutf8.sql[world].

=== Exercicis

[NOTE]
====
Per a cada exercici es vol:

- el codi que crea la funció.
- un exemple de com s'utilitza.
- el resultat obtingut.
====

[IMPORTANT]
====
Cal utilitzar com a mínim un cursor a cadascun dels exercicis.
====

1. Escriu una funció que rebi el codi d'un país i mostri el nom dels idiomes que
s'hi parlen, indicant per a cadascun si és oficial o no. Utilitza un bucle
`WHILE`.

2. Repeteix l'exercici anterior, però utilitzant un bucle `FOR`.

3. Fes una funció que mostri les ciutats d'un país. Es rep el codi del país per
paràmetre. Ha de mostrar les ciutats ordenades alfabèticament, i numerant cada
línia.

4. Modifica l'exercici anterior per fer que totes les ciutats del país es
retornin en un conjunt, però que se'n mostrin per pantalla un màxim de 5. Si
n'hi ha més, al final de la llista es mostraran uns punts suspensius.

5. Fes una funció que rebi un nombre enter. La funció agafarà tots els noms de
país ordenats alfabèticament i en retornarà només aquells que ocupen una
posició múltiple del nombre rebut. Per exemple, si el nombre és 3, retornarà el
tercer nom, el sisè, el novè, etc.

6. Fes una funció que calculi quants països d'una regió donada tenen una
esperança de vida superior a la mitjana del seu continent. La funció ha
de retornar NULL si la regió no existeix.

7. Fes una funció que, utilitzant la funció de l'exercici anterior, mostri per
pantalla el nom de les regions on menys de la meitat dels països tenen una
esperança de vida superior a la mitjana del seu continent.

8. Escriu una funció que rebi el nom d'una regió i el percentatge d'increment
del producte interior brut (`gnp`). La funció incrementarà el PIB de cada país
de la regió en el tant per cent indicat i retornarà la quantitat de països
modificats. Per a cada país es mostrarà per pantalla el seu codi i nom, i el PIB
abans i després de la modificació. El percentatge d'increment es rebrà en tant
per ú, és a dir, per exemple per un 10% es rebria 0,1.

9. Escriu una funció que rebi un nombre enter positiu, _n_. La funció repetirà
la següent operació _n_ cops. A cada iteració, ha d'agafar el país amb el PIB
més alt i restar-li un 10%. Aquesta quantitat s'ha de repartir a parts iguals
entre els _n_ països més pobres, els que tenen el PIB més baix. S'ha de mostrar
per pantalla les modificacions que es van fent.

10. Fes una funció que apugi el PIB de tots els països que el tinguin per sota
de la mitjana dels països de la seva regió. La pujada serà d'un 30% de la
diferència entre el seu PIB i la mitjana de la regió.
