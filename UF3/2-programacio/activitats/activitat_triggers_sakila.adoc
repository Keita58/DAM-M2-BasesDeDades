= Programació en bases de dades
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

== Activitat triggers sobre sakila

Utilitzarem la base de dades link:db/postgresql/sakila.sql[sakila].

=== Exercicis

[NOTE]
====
Per a cada exercici es vol:

- el codi que crea la funció.
- un exemple de com s'utilitza.
- el resultat obtingut.
====

1. Per calcular la data màxima de retorn d'un lloguer hem d'afegir
a la data de lloguer (`rental_date` de la taula `rental`) el número de dies que
es permet llogar una pel·lícula (`rental_duration` de la taula `film`).
+
Això pot ser una problema si es vol modificar en algun moment el camp
`rental_duration` d'una pel·lícula, ja que no podríem saber la data límit de
retorn dels lloguers que en aquell moment estiguessin pendents.
+
Per arreglar-ho, afegirem un nou camp a la taula `rental` on hi consti la data
màxima de retorn, i que es calcularà en el moment de llogar la pel·lícula.
D'aquesta manera, la data màxima de retorn queda guardada a cada lloguer i no
es veu afectada per canvis posteriors a `rental_duration`.
+
Per afegir el nou camp utilitzarem l'ordre:
+
[source,sql]
----
ALTER TABLE rental ADD COLUMN max_return_date DATE DEFAULT NULL;
----
+
El següent pas és fer que aquest camp es calculi automàticament cada vegada
que es crea o es modifica un lloguer.
+
Crea un trigger que s'activi cada vegada que s'insereixi un nou lloguer o es
modifiqui la data de lloguer d'un lloguer existent.
+
Quan s'activi el trigger s'actualitzarà el camp `max_return_date` a la
data de retorn adequada.
+
Comprova el funcionament del trigger ens els diversos casos possibles: quan
s'insereix un nou lloguer, i quan s'actualitza un lloguer existent.
+
[TIP]
====
Per sumar un cert nombre de dies `x` a una data `d` i guardar-ho a la data `df`:

----
df := d + x*'1 day'::interval;
----
====

2. El control de l'inventari és una mica complicat ara mateix: per saber si una
pel·lícula es troba disponible, cal mirar a la taula `rental` si existeix algun
lloguer d'aquell ítem que no s'hagi retornat. A més, és possible que algun ítem
no estigui disponible per altres motius: reparació, perdut, etc.
+
[TIP]
====
Per veure com crear un tipus enum en PostgreSQL, mira l'script de creació de
sakila i/o la documentació oficial.
====
+
Anem a arreglar això, fent que cada ítem tingui un estat. Crea un tipus de dada
enum amb els valors possibles _available_, _rent_, _maintenance_, _lost_,
_others_.
+
Crea una columna nova, _status_, a la taula _inventory_ del tipus de l'enum anterior.
Inicialitza cada fila al seu valor: _available_ si la pel·lícula no està
llogada, i _rent_ si ho està.
+
Crea el triggers necessari per no permetre que una pel·lícula es llogui si no
està disponible. Assegura't que el trigger mostra un missatge o llanci una
excepció que deixi clar el motiu de no permetre el canvi.
+
Crea el trigger necessari per no permetre que una pel·lícula es retorni si no
està llogada. Mostra un missatge adequat com a l'apartat anterior.
+
Crea els triggers necessaris per mantenir la columna _status_: que l'estat sigui
_rent_ quan es lloga, i que sigui _available_ quan es retorna.
