= Programació en bases de dades
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

== Activitat funcions PL/pgSQL bàsiques

Utilitzarem la base de dades link:db/postgresql/worldutf8.sql[world].

=== Exercicis

[NOTE]
====
Per a cada exercici es vol:

- el codi que crea la funció.
- un exemple de com s'utilitza.
- el resultat obtingut.
====

1. Fes una funció que rebi com a paràmetres dos nombres, i retorni la seva suma.

2. Fes una funció que rebi com a paràmetre dues cadenes de text i mostri un
missatge indicant la longitud de cadascuna d'elles. La funció retornarà la
longitud de les dues cadenes sumada.

3. Fes una funció que rebi el nom d'un idioma i retorni a quants països es parla
aquest idioma. Aquesta funció ja l'hem feta en versió SQL, però ara la volem
amb PL/pgSQL.

4. Fes una funció que permeti inserir noves ciutats a la base de dades. La
funció rebrà el nom de la ciutat, el codi del país, el districte i la quantitat
d'habitants. Ha de retornar l'id de la ciutat creada. Aquesta funció ja l'hem
feta en versió SQL, però ara la volem amb PL/pgSQL.

5. Fes una funció que retorni la quantitat total de files que hi ha entre totes
les taules de la base de dades.

6. Fes una funció que rebi el codi d'un país, i retorni la ciutat més poblada
del país. Fes que es produeixi un error si el codi del país no existeix (o es
tracta d'un país sense cap ciutat).

7. Fes una funció que retorni la quantitat de files que hi ha en cada taula de
la base de dades, en tres paràmetres de sortida.

8. Fes una funció que rebi el nom d'un idioma i retorni tots els països on es
parla aquest idioma. Utilitza `RETURN QUERY`. A més, la funció mostrarà per
pantalla a quants països es parla l'idioma en qüestió.
