= Programació en bases de dades
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:
:ascii-ids:

<<<

== Activitat funcions PL/pgSQL sobre sakila

Utilitzarem la base de dades link:db/postgresql/sakila.sql[sakila].

=== Exercicis

[NOTE]
====
Per a cada exercici es vol:

- el codi que crea la funció.
- un exemple de com s'utilitza.
- el resultat obtingut.
====

1. Fes una funció que rebi com a paràmetres dues durades en minuts i compti la
quantitat de pel·lícules que tenen una durada que està entre aquestes dues. La
funció retornarà aquesta quantitat. Si no n'hi ha cap, a més, mostrarà un missatge.
+
[source,sql]
----
create or replace function films_by_length(min_length int, max_length int) returns int as $$
declare
  n_films int;
begin
  select count(*) into n_films from film
  where length between min_length and max_length;
  if n_films=0 then
    raise notice 'No hi ha cap pel·lícula entre % i % minuts.', min_length, max_length;
  end if;
  return n_films;
end;
$$ language plpgsql;

select films_by_length(30, 50);
films_by_length|
---------------+
             37|
----

2. Crea una funció que rebi el nom i cognom d'un actor o actriu i que retorni
totes les dades de les pel·lícules on ha participat. Ha de mostrar errors
tant si no existeix l'actor com si no ha fet cap pel·lícula.
+
[source,sql]
----
create or replace function films_by_actor(firstn text, lastn text) returns setof film as $$
declare
  n_actors int;
begin
  select count(*) into n_actors from actor
  where first_name=firstn and last_name=lastn;
  if n_actors=0 then
    raise exception 'No existeix l''actor.';
  elsif n_actors=0 then
    raise warning 'Hi ha més d''un actor amb aquest nom i cognom.';
  end if;
  return query select f.*
  from film f
  join film_actor fa on fa.film_id=f.film_id
  join actor a on a.actor_id=fa.actor_id
  where a.first_name=firstn and a.last_name=lastn;
  if not found then
    raise warning 'Aquest actor no ha fet cap pel·lícula.';
  end if;
end;
$$ language plpgsql;

select * from films_by_actor('PENELOPE', 'GUINESS');
film_id|title                |description                                                                                                            |release_year|language_id|original_language_id|rental_duration|rental_rate|length|replacement_cost|rating|special_features                                        |last_update                  |
-------+---------------------+-----------------------------------------------------------------------------------------------------------------------+------------+-----------+--------------------+---------------+-----------+------+----------------+------+--------------------------------------------------------+-----------------------------+
      1|ACADEMY DINOSAUR     |A Epic Drama of a Feminist And a Mad Scientist who must Battle a Teacher in The Canadian Rockies                       |        2006|          1|                    |              6|       0.99|    86|           20.99|PG    |{Deleted Scenes,Behind the Scenes}                      |2006-02-15 06:03:42.000 +0100|
     23|ANACONDA CONFESSIONS |A Lacklusture Display of a Dentist And a Dentist who must Fight a Girl in Australia                                    |        2006|          1|                    |              3|       0.99|    92|            9.99|R     |{Trailers,Deleted Scenes}                               |2006-02-15 06:03:42.000 +0100|
     25|ANGELS LIFE          |A Thoughtful Display of a Woman And a Astronaut who must Battle a Robot in Berlin                                      |        2006|          1|                    |              3|       2.99|    74|           15.99|G     |{Trailers}                                              |2006-02-15 06:03:42.000 +0100|
    106|BULWORTH COMMANDMENTS|A Amazing Display of a Mad Cow And a Pioneer who must Redeem a Sumo Wrestler in The Outback                            |        2006|          1|                    |              4|       2.99|    61|           14.99|G     |{Trailers}                                              |2006-02-15 06:03:42.000 +0100|
    140|CHEAPER CLYDE        |A Emotional Character Study of a Pioneer And a Girl who must Discover a Dog in Ancient Japan                           |        2006|          1|                    |              6|       0.99|    87|           23.99|G     |{Trailers,Commentaries,Behind the Scenes}               |2006-02-15 06:03:42.000 +0100|
    166|COLOR PHILADELPHIA   |A Thoughtful Panorama of a Car And a Crocodile who must Sink a Monkey in The Sahara Desert                             |        2006|          1|                    |              6|       2.99|   149|           19.99|G     |{Commentaries,Behind the Scenes}                        |2006-02-15 06:03:42.000 +0100|
    277|ELEPHANT TROJAN      |A Beautiful Panorama of a Lumberjack And a Forensic Psychologist who must Overcome a Frisbee in A Baloon               |        2006|          1|                    |              4|       4.99|   126|           24.99|PG-13 |{Behind the Scenes}                                     |2006-02-15 06:03:42.000 +0100|
    361|GLEAMING JAWBREAKER  |A Amazing Display of a Composer And a Forensic Psychologist who must Discover a Car in The Canadian Rockies            |        2006|          1|                    |              5|       2.99|    89|           25.99|NC-17 |{Trailers,Commentaries}                                 |2006-02-15 06:03:42.000 +0100|
    438|HUMAN GRAFFITI       |A Beautiful Reflection of a Womanizer And a Sumo Wrestler who must Chase a Database Administrator in The Gulf of Mexico|        2006|          1|                    |              3|       2.99|    68|           22.99|NC-17 |{Trailers,Behind the Scenes}                            |2006-02-15 06:03:42.000 +0100|
    499|KING EVOLUTION       |A Action-Packed Tale of a Boy And a Lumberjack who must Chase a Madman in A Baloon                                     |        2006|          1|                    |              3|       4.99|   184|           24.99|NC-17 |{Trailers,Deleted Scenes,Behind the Scenes}             |2006-02-15 06:03:42.000 +0100|
    506|LADY STAGE           |A Beautiful Character Study of a Woman And a Man who must Pursue a Explorer in A U-Boat                                |        2006|          1|                    |              4|       4.99|    67|           14.99|PG    |{Trailers,Deleted Scenes,Behind the Scenes}             |2006-02-15 06:03:42.000 +0100|
    509|LANGUAGE COWBOY      |A Epic Yarn of a Cat And a Madman who must Vanquish a Dentist in An Abandoned Amusement Park                           |        2006|          1|                    |              5|       0.99|    78|           26.99|NC-17 |{Trailers,Deleted Scenes}                               |2006-02-15 06:03:42.000 +0100|
    605|MULHOLLAND BEAST     |A Awe-Inspiring Display of a Husband And a Squirrel who must Battle a Sumo Wrestler in A Jet Boat                      |        2006|          1|                    |              7|       2.99|   157|           13.99|PG    |{Trailers,Deleted Scenes,Behind the Scenes}             |2006-02-15 06:03:42.000 +0100|
    635|OKLAHOMA JUMANJI     |A Thoughtful Drama of a Dentist And a Womanizer who must Meet a Husband in The Sahara Desert                           |        2006|          1|                    |              7|       0.99|    58|           15.99|PG    |{Behind the Scenes}                                     |2006-02-15 06:03:42.000 +0100|
    749|RULES HUMAN          |A Beautiful Epistle of a Astronaut And a Student who must Confront a Monkey in An Abandoned Fun House                  |        2006|          1|                    |              6|       4.99|   153|           19.99|R     |{Deleted Scenes,Behind the Scenes}                      |2006-02-15 06:03:42.000 +0100|
    832|SPLASH GUMP          |A Taut Saga of a Crocodile And a Boat who must Conquer a Hunter in A Shark Tank                                        |        2006|          1|                    |              5|       0.99|   175|           16.99|PG    |{Trailers,Commentaries,Deleted Scenes,Behind the Scenes}|2006-02-15 06:03:42.000 +0100|
    939|VERTIGO NORTHWEST    |A Unbelieveable Display of a Mad Scientist And a Mad Scientist who must Outgun a Mad Cow in Ancient Japan              |        2006|          1|                    |              4|       2.99|    90|           17.99|R     |{Commentaries,Behind the Scenes}                        |2006-02-15 06:03:42.000 +0100|
    970|WESTWARD SEABISCUIT  |A Lacklusture Tale of a Butler And a Husband who must Face a Boy in Ancient China                                      |        2006|          1|                    |              7|       0.99|    52|           11.99|NC-17 |{Commentaries,Deleted Scenes}                           |2006-02-15 06:03:42.000 +0100|
    980|WIZARD COLDBLOODED   |A Lacklusture Display of a Robot And a Girl who must Defeat a Sumo Wrestler in A MySQL Convention                      |        2006|          1|                    |              4|       4.99|    75|           12.99|PG    |{Commentaries,Deleted Scenes,Behind the Scenes}         |2006-02-15 06:03:42.000 +0100|

select * from films_by_actor('a', 'a');
SQL Error [P0001]: ERROR: No existeix l''actor.
  Where: PL/pgSQL function films_by_actor(text,text) line 8 at RAISE
----

3. Fes una funció que pugui afegir un idioma a una pel·lícula en concret. Es passarà
per paràmetres el títol de la pel·lícula i l'idioma. La funció informarà dels diferents
errors que poden sortir.
+
[source,sql]
----
create or replace function add_language(ftitle text, flang text) returns void as $$
declare
  f film;
  lid int;
begin
  select * into f from film
  where title=ftitle and original_language_id is null;
  if not found then
    raise exception 'No existeix aquesta pel·lícula.';
  end if;
  select language_id into lid from language where name = flang;
  if not found then
    raise exception 'No existeix aquest idioma.';
  end if;
  perform * from film where title=ftitle and language_id=lid;
  if found then
    raise exception 'Aquesta pel·lícula ja existeix en aquest idioma.';
  end if;
  f.original_language_id:=f.language_id;
  f.language_id:=lid;
  insert into film(title, description, release_year, language_id, original_language_id,
    rental_duration, rental_rate, length, replacement_cost, rating, special_features)
    values (f.title, f.description, f.release_year, f.language_id, f.original_language_id,
    f.rental_duration, f.rental_rate, f.length, f.replacement_cost, f.rating, f.special_features);
end;
$$ language plpgsql;

select add_language('ACADEMY DINOSAUR', 'French');

select film_id, language_id, original_language_id from film where title='ACADEMY DINOSAUR';
film_id|language_id|original_language_id|
-------+-----------+--------------------+
      1|          1|                    |
   1001|          5|                   1|

select add_language('ACADEMY DINOSAUR', 'French');
SQL Error [P0001]: ERROR: Aquesta pel·lícula ja existeix en aquest idioma.
  Where: PL/pgSQL function add_language(text,text) line 17 at RAISE
----
