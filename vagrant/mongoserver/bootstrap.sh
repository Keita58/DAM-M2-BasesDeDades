#!/bin/sh

# Instal·lació mongodb
apt-get update
apt-get install -y gnupg curl
curl -fsSL https://www.mongodb.org/static/pgp/server-7.0.asc | \
   gpg -o /usr/share/keyrings/mongodb-server-7.0.gpg \
   --dearmor
echo "deb [ signed-by=/usr/share/keyrings/mongodb-server-7.0.gpg ] http://repo.mongodb.org/apt/debian bookworm/mongodb-org/7.0 main" | tee /etc/apt/sources.list.d/mongodb-org-7.0.list
apt-get update
apt-get install -y mongodb-org

systemctl enable mongod
systemctl start mongod

# Importa col·lecció books
mongoimport -d books -c books < /db/books.json
# Importa col·lecció zips
wget -nv https://media.mongodb.org/zips.json
mongoimport -d zips -c zips < zips.json
# Importa col·lecció countries
wget -nv https://github.com/mledoze/countries/raw/master/countries.json
mongoimport -d countries -c countries --jsonArray < countries.json

