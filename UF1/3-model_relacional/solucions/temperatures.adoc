= Dades meteorològiques
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:

<<<

== Model relacional

*Countries*([underline]#Name#, Area, Population, Climate).

*Cities*([underline]#Name, Country#, Longitude, Latitude) +
{nbsp}{nbsp}on _Country_ referencia _Countries_.

*Weather*([underline]#City, Country, Datetime#, Temperature, Rain, Humidity) +
{nbsp}{nbsp}on _City_ i _Country_ referencia _Cities_.
