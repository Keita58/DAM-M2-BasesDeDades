= El concessionari
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:

<<<

== Model relacional

*Mechanics*([underline]#DNI#, FirstName, LastName)

*Cars*([underline]#ChassisNumber#, PlateNumber, Manufacturer, Model, FabricationYear)

*Sellers*([underline]#DNI#, FirstName, LastName)

*Customers*([underline]#DNI#, FirstName, LastName)

*Services*([underline]#ChassisNumber, StartDate#, PickupDate, CustomerDNI) +
{nbsp}{nbsp}on _ChassisNumber_ referencia _Cars_ i +
{nbsp}{nbsp}on _CustomerDNI_ referencia _Customers_.

*Parts*([underline]#RefNumber#, Name, Model, Manufacturer, ChassisNumber, ServiceStartDate)
{nbsp}{nbsp}on _ChassisNumber_ i _ServiceStartDate_ referencien _Services_.

*MechanicServices*([underline]#MechanicDNI, ChassisNumber, StartDate#, Time) +
{nbsp}{nbsp}on _MechanicDNI_ referencia _Mechanics_ i +
{nbsp}{nbsp}on _ChassisNumber_ i _StartDate_ referencien _Services_.

*Sells*([underline]#CustomerDNI, ChassisNumber#, SellerDNI, Date, Price) +
{nbsp}{nbsp}on _CustomerDNI_ referencia _Customers_, +
{nbsp}{nbsp}on _SellerDNI_ referencia _Sellers_, i +
{nbsp}{nbsp}on _ChassisNumber_ referencia _Cars_.
