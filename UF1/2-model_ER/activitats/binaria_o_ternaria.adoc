= Relacions binàries o ternàries
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:

<<<

Cadascuna de les següents descripcions es correspon a una relació binària,
dues relacions binàries, una relació ternària, o una relació quaternària. Indica
en cada cas de quin tipus de relació es tracta i justifica la resposta.

1. En un joc hi ha una sèrie de territoris. Cada jugador disposa de diverses
unitats, cadascuna de les quals té un nom únic que la identifica. Cada jugador
pot tenir les seves unitats distribuïdes entre els diversos territoris. En un
territori hi poden haver unitats de diversos jugadors. Com és la relació entre
els jugadors, els territoris, i les unitats?

2. En un joc hi ha una sèrie de territoris neutrals i, cada torn, els jugadors
poden atacar-los. Volem guardar un registre dels atacs que els jugadors fan als
territoris al llarg de la partida. Com és la relació?

3. En un joc hi ha una sèrie de territoris. Cada territori pertany a un jugador,
el qual pot tenir diverses unitats de diferents tipus en els seus territoris.
Com és la relació entre els jugadors, els territoris, i les unitats?

4. En un joc hi ha una sèrie de territoris. Cada territori pertany a un jugador,
el qual el pot vendre a un altre jugador. Volem guardar l'històric de ventes que
s'han fet al llarg d'una partida. Per cada venta volem saber el jugador que ven,
el jugador que compra, el territori venut, el preu de venta, i el torn en què
s'ha fet. Com és la relació?

5. En un joc hi ha una sèrie de territoris. Al llarg dels torn, es produeixen
esdeveniments aleatoris en aquests territoris. Volem guardar un registre de
quins esdeveniments s'han produït sobre quins territoris i en quins torns.
Com és la relació?

6. En un joc hi ha una sèrie de territoris. A cada territori es produiran els
mateixos esdeveniments, un cop cadascun, però en ordre i torns diferents per
cada territori. Volem guardar en quin torn s'ha produït cada esdeveniment a
cadascun dels territoris. Com és la relació?

7. En un joc hi ha una sèrie de territoris. Cada jugador exerceix una influència
sobre cadascun dels territoris. El jugador que en un moment donat exerceix més
influència controla el territori. Cada torn necessitem saber el nivell
d'influència que exerceix cada jugador a cada territori (no guardem històric).
Com és la relació?

8. En un joc hi ha una sèrie de territoris. Sobre aquests territoris els
jugadors activen efectes que els beneficien i que duren diversos torns. Diversos
jugadors poden activar el mateix efecte. Necessitem saber en un moment donat
(no hi ha històric) quins efectes afecten a cada territori, durant quants torns,
i quin jugador els ha activat. Com és la relació?
