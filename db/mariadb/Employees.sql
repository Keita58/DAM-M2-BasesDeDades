drop database if exists company;
create database company;
use company;
create table Employees (
	Id INT UNSIGNED PRIMARY KEY,
	FirstName VARCHAR(100) NOT NULL,
	LastName VARCHAR(100) NOT NULL,
	EMail VARCHAR(100),
	Phone CHAR(12),
	JobTitle VARCHAR(100) NOT NULL,
    BirthDate DATE NOT NULL,
    Salary INT UNSIGNED
);
insert into Employees (Id, FirstName, LastName, EMail, Phone, JobTitle, BirthDate, Salary) values (1, 'Meade', 'Twigg', 'mtwigg0@nymag.com', '796-159-7480', 'Help Desk Technician', '1990-02-15', 1400);
insert into Employees (Id, FirstName, LastName, EMail, Phone, JobTitle, BirthDate, Salary) values (2, 'Amargo', 'Heathwood', 'aheathwood1@ow.ly', '225-923-8090', 'Legal Assistant', '1975-09-23', 1900);
insert into Employees (Id, FirstName, LastName, EMail, Phone, JobTitle, BirthDate, Salary) values (3, 'Christie', 'Cutmere', 'ccutmere2@wikimedia.org', '350-605-5247', 'Tax Accountant', '1982-11-01', 2100);
insert into Employees (Id, FirstName, LastName, EMail, Phone, JobTitle, BirthDate, Salary) values (4, 'Reena', 'Polycote', NULL, '729-706-3654', 'Civil Engineer', '1984-08-12', 1800);
insert into Employees (Id, FirstName, LastName, EMail, Phone, JobTitle, BirthDate, Salary) values (5, 'Horst', 'Laybourn', 'hlaybourn4@usatoday.com', '413-491-3518', 'Software Consultant', '1986-09-21', 1800);
insert into Employees (Id, FirstName, LastName, EMail, Phone, JobTitle, BirthDate, Salary) values (6, 'Jo', 'Ledgeway', 'jledgeway5@gnu.org', '566-264-7812', 'Accountant', '1978-03-06', 2200);
insert into Employees (Id, FirstName, LastName, EMail, Phone, JobTitle, BirthDate, Salary) values (7, 'Samuele', 'Durdan', 'sdurdan6@dailymail.co.uk', NULL, 'Software Engineer', '1980-02-07', 1900);
insert into Employees (Id, FirstName, LastName, EMail, Phone, JobTitle, BirthDate, Salary) values (8, 'Beatrice', 'Lodovichi', 'blodovichi7@nasa.gov', '608-742-5611', 'Software Engineer', '1992-11-30', 2100);
insert into Employees (Id, FirstName, LastName, EMail, Phone, JobTitle, BirthDate, Salary) values (9, 'Phil', 'Rizzi', 'prizzi8@ca.gov', '785-787-4204', 'Financial Analyst', '1989-05-26', 2200);
insert into Employees (Id, FirstName, LastName, EMail, Phone, JobTitle, BirthDate, Salary) values (10, 'Perry', 'Knell', 'pknell9@mozilla.com', '610-171-0084', 'Software Engineer', '1972-06-29', 2300);
